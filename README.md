Mobile Application Programming, SCSJ3623
School of Computing, UTM
June 2020

# Group Project Final Version

## Project Title: Food Ordering Application

## Project Synopsis:

................................................................................................................

_Note: Briefly describe what your app is all about_

## Group Members
- Name 1: KANG WEI KIAT
- Name 2: KEE KELVEN
- Name 3: WONG SING HUA
- Name 4: WONG LIT KWONG

## Video URL: ........................

_Note:_

- Video must be uploaded to youtube.
- Provide timestamps to your video.

## REST API URL/Project URL on Firebase: https://us-central1-coconut-backend-food-app.cloudfunctions.net/api 

## Git Repo URL on GitHub (front-end): ........................

## Git Repo URL on GitHub (back-end) : ........................

## User credential (username and passwords) to test out your app:
1. user's credential: N/A
2. admin's credential 
Username: kangweikiat@gmail.com
Password: kangweikiat

